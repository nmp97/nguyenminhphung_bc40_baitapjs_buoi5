// EXERCISE 1
var btnResult = document.getElementById('btn-result');

function calcScoreArea(scoreArea) {
  switch (scoreArea) {
    case '':
      return 0;
    case 'A':
      return 2;
    case 'B':
      return 1;
    case 'C':
      return 0.5;
  }
}

function calcScoreObject(scoreObject) {
  switch (scoreObject) {
    case '':
      return 0;
    case '1':
      return 2.5;
    case '2':
      return 1.5;
    case '3':
      return 1;
  }
}

function calcScore(score1, score2, score3) {
  return score1 + score2 + score3;
}

btnResult.addEventListener('click', function () {
  var scoreArea = document.getElementById('area').value;
  var scoreObject = document.getElementById('object').value;
  var score1 = document.getElementById('grade1__input').value * 1;
  var score2 = document.getElementById('grade2__input').value * 1;
  var score3 = document.getElementById('grade3__input').value * 1;
  var benchMark = document.getElementById('bench-mark').value * 1;
  var contentHTML = '';

  if (0 < benchMark && benchMark <= 30) {
    let result =
      calcScore(score1, score2, score3) + calcScoreArea(scoreArea) + calcScoreObject(scoreObject);
    if (result >= benchMark) {
      contentHTML += `Chúc mừng! Bạn đã đậu. Tổng điểm: ${result}`;
    }
    if (result < benchMark) {
      contentHTML += `Rất tiếc! Bạn đã rớt. Tổng điểm: ${result}`;
    }
  } else {
    alert('Vui lòng nhập đúng điểm chuẩn');
  }

  document.getElementById('result').innerHTML = contentHTML;
});

//EXERCISE 2
var btnEx2 = document.getElementById('ex2__btn');

// Format Number as String functions
var numberFormat = new Intl.NumberFormat('vi-VN', {
  style: 'currency',
  currency: 'VND',
  currencyDisplay: 'code',
});

btnEx2.addEventListener('click', function () {
  const PRICE_1KWH_TO_50KWH = 500;
  const PRICE_51KWH_TO_100KWH = 650;
  const PRICE_101KWH_TO_200KWH = 850;
  const PRICE_201KWH_TO_350KWH = 1100;
  const PRICE_FROM_351KWH = 1300;

  let nameEx2 = document.getElementById('ex2__name').value;
  let Kwh = document.getElementById('ex2__kWh').value * 1;
  let total = 0;

  if (nameEx2) {
    if (Kwh) {
      if (Kwh <= 50) {
        total += Kwh * PRICE_1KWH_TO_50KWH;
      } else if (Kwh <= 100) {
        total += 50 * PRICE_1KWH_TO_50KWH + (Kwh - 50) * PRICE_51KWH_TO_100KWH;
      } else if (Kwh <= 200) {
        total +=
          50 * PRICE_1KWH_TO_50KWH +
          50 * PRICE_51KWH_TO_100KWH +
          (Kwh - 100) * PRICE_101KWH_TO_200KWH;
      } else if (Kwh <= 350) {
        total +=
          50 * PRICE_1KWH_TO_50KWH +
          50 * PRICE_51KWH_TO_100KWH +
          100 * PRICE_101KWH_TO_200KWH +
          (Kwh - 200) * PRICE_201KWH_TO_350KWH;
      } else if (Kwh > 350) {
        total +=
          50 * PRICE_1KWH_TO_50KWH +
          50 * PRICE_51KWH_TO_100KWH +
          100 * PRICE_101KWH_TO_200KWH +
          150 * PRICE_201KWH_TO_350KWH +
          (Kwh - 350) * PRICE_FROM_351KWH;
      }
    } else {
      return alert('Vui lòng nhập số kw điện đã tiêu thụ');
    }
  } else {
    return alert('Vui lòng nhập tên của bạn');
  }

  document.getElementById(
    'ex2__result'
  ).innerHTML = `Họ và tên: ${nameEx2} - Tổng tiền điện: ${numberFormat.format(total)}`;
});
